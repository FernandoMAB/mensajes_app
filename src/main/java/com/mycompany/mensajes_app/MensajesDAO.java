/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.Timestamp;  
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

/**
 *
 * @author fersi
 */
public class MensajesDAO {
    
    public static void CrearMensajeDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try (Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try {
                String query = "INSERT INTO `mensajes` (mensaje, autor_mensaje, fecha_mensaje) VALUES (?,?,?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                Timestamp instant= Timestamp.from(Instant.now());
                String fecha= instant.toString();
                ps.setString(3, fecha);
                
                ps.executeUpdate(); //hace la actualizacion de los datos
                System.out.println("Mensaje creado");
                
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            
            
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
    }
    
    public static void leerMensajesDB(){
        Conexion db_connect = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;    //nos permite traer los datos en filas
        
        try (Connection conexion = db_connect.get_connection()){
            String query = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();  //ejecutar sentencia
            
            while (rs.next()) { 
                System.out.println("ID: " + rs.getInt("id_mensaje"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getString("fecha_mensaje"));
                System.out.println("----------------------------------------");
                
            }
                
                
                 
        } catch (SQLException e) {
            System.out.println("No se pudieron recuperar los mensajes");
            System.out.println(e);
        }
    }
    
    public static void borrarMensajeDB(int id_mensaje){
        Conexion db_connect = new Conexion();
        
        try (Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate(); //hace la actualizacion de los datos, es una operacion de transaccion
                System.out.println("Mensaje eliminado");
                
            } catch (SQLException ex) {
                System.out.println("No se pudieron eliminar el mensaje");
                System.out.println(ex);
            }        
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void actualizarMensajeDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try (Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try {
                String query = "UPDATE mensajes SET mensaje = ?, fecha_mensaje = ? WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                Timestamp instant= Timestamp.from(Instant.now());
                String fecha= instant.toString();
                ps.setString(2, fecha);
                ps.setInt(3, mensaje.getId_mensaje());
                ps.executeUpdate(); //hace la actualizacion de los datos, es una operacion de transaccion
                System.out.println("Mensaje actualizado");
                
            } catch (SQLException ex) {
                System.out.println("No se pudo actualizar el mensaje");
                System.out.println(ex);
            }        
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
}
